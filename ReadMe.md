# Installer le projet
- [Télécharger](https://www.python.org/downloads/release/python-369/) et installer python 3.6.9 ou une version compatible avec celle-ci
- Dans une invite de commande, placez vous dans le repertoire du projet
- Lancer la commande `pip install -r requirements.txt`

# Lancer le projet
- Lancer le docker-compose afin de lancer Elastic Search et Kibana
- Dans une invite de commande, placez vous dans le repertoire du projet
- Executer le script `Uploader.py`:
  - dans l'invite de commande de **Windows** lancer la commande: `pathToPython\python.exe Uploader.py`
  - dans l'invite de commande de  **Linux** lancer la commande: `python Uploader.py`
  - dans l'invite de commande de  **Mac OS X** lancer la commande: `python Uploader.py`