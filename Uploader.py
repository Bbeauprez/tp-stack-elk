from elasticsearch import Elasticsearch

es = Elasticsearch(
    ['localhost'],
    port=9200
)
# le csv fournis devant etre ouvert dans un excel pour etre traité comme tel par les librairie
# j'ai choisi de parser a la main plutot que d'imposer cette ouverture avant de pouvoir utiliser le script
with open('files/blog.csv') as csvFile:
    lines = csvFile.readlines()
    count = 1
    for line in lines:
        parsedLine = line.split(';')
        countColumn = len(parsedLine)

        if countColumn == 8:
            data = {
                'title': parsedLine[0],
                'seo_title': parsedLine[1],
                'url': parsedLine[2],
                'author': parsedLine[3],
                'date': parsedLine[4],
                'category': parsedLine[5],
                'locales': parsedLine[6],
                'content': parsedLine[7]
            }
            es.index(index='groupe3', doc_type='_doc', id=count, body=data)
        else:
            print('missing data at line: {}, ignored line n°'.format(count))
        count += 1
print('processed {} lines'.format(count - 1))
